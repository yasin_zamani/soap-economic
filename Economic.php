
<button onclick="exportTableToCSV('orders.csv')">Export HTML Table To CSV File</button>
<button onclick="exportTableToExcel('tblData')">Export Table Data To Excel File</button>
<?php
$wsdlUrl = 'https://api.e-conomic.com/secure/api1/EconomicWebservice.asmx?WSDL';
    $client = new SoapClient(
		$wsdlUrl,
		array(
			"trace" => 1,
			"exceptions" => 1,
			"features" => SOAP_SINGLE_ELEMENT_ARRAYS
		)
	);
	$client->ConnectWithToken(
		array(
			'token' 	=> "CHANGEME",
			'appToken'	=> "CHANGEME"
		)
	);



    $debtorHandles = $client->Order_GetAll()->Order_GetAllResult->OrderHandle;
	$debtorDataObjects = $client ->Order_GetDataArray(array('entityHandles'=>array('OrderHandle'=>$debtorHandles)))->Order_GetDataArrayResult->OrderData;
    /*
    $invoicesHandle = $client->Invoice_GetAll()->Invoice_GetAllResult->InvoiceHandle;
    $Invoices = $client->Invoice_GetDataArray(array('entityHandles'=>array('InvoiceHandle'=>$invoicesHandle)))->Invoice_GetDataArrayResult->InvoiceData;
    */
    
    $handle_array = $client->OrderLine_GetAll(array('OrderLineHandle'=>array($debtorHandles)))->OrderLine_GetAllResult->OrderLineHandle;
    $data_product = $client->OrderLine_GetDataArray(array('entityHandles'=>$handle_array))->OrderLine_GetDataArrayResult->OrderLineData;
    $productNO = ($data_product[15]->ProductHandle->Number);
    $productDESC = $data_product[20]->Description;
    //var_dump($data_product[0]);
    $array_of_order_lines = array();
    foreach($data_product as $d) {
        //var_dump($d->Handle);
        $array_of_ord = array(
            isset($d->Handle->Id) ? $d->Handle->Id : '',
            isset($d->ProductHandle->Number) ? $d->ProductHandle->Number : '',
            isset($d->Description) ? $d->Description : '',
            isset($d->Quantity) ? $d->Quantity : '',
            isset($d->UnitNetPrice) ? $d->UnitNetPrice : '',

        );
        //var_dump($d->Quantity);
        $array_of_order_lines[] = $array_of_ord; 
        
    }
    //var_dump($array_of_order_lines);
    $orderreport = array();
    $xd = 0;
    $count;
    define('IDX_ORDERS', array(
        'DebtorName' => 0,
        'OrderNumber' => 1,
        'DebtorAddress' => 2,
        'NetAmount' => 3,
        'Heading' => 4,
        'Id' => 5,
        'ProductNO' => 6,
        'ProductDesc' => 7,
        'ProductQuantity' => 8,
        'ProductUnitPrice' => 9,
        'Date' => 10,
        'IsArchived' => 11,
        'IsSent' => 12,
    ));
    
    foreach ($debtorDataObjects as $d) {
        $xd++;
        if($xd >= 1) {
            $MergeData = $array_of_order_lines;
            foreach($MergeData as $p) {
                
                if($p[0] == $d->Id) {
                  
                    $orderreportline = array(
                        $d->DebtorName,
                        $d->Number,
                        $d->DebtorAddress,
                        $d->NetAmount,
                        isset($d->Heading) ? $d->Heading : '',
                        $d->Id,
                        $p[1],
                        $p[2],
                        $p[3],
                        $p[4],
                        $d->Date,
                        $d->IsArchived,
                        $d->IsSent
                    );
                    $orderreport[] = $orderreportline;
                }

               //
                 
                
            }
            $xd++;
        }
}


echo "<table border='1' id=tblData>

<tr>
<th>Date</th>
<th>OrderId</th>
<th>Name</th>

<th>ProductNo</th>
<th>Description</th>
<th>Unit Price</th>
<th>Quantity</th>
<th>Total Order</th>
<th>IsArchived</th>
<th>IsSent</th>
<th>Fakturert?</th>

</tr>";
$sum = 0;
$iAux;
foreach ($orderreport as $t) {
    $date =  $t[IDX_ORDERS['Date']];
    $isSent =  $t[IDX_ORDERS['IsSent']];
    $isArchived =  $t[IDX_ORDERS['IsArchived']];
    if($isSent && $isArchived ) {
       $iAux = "Fakturert"; 
    } else {
        $iAux = "";
    }
    $a = strtotime($date);
    $new = date("d/m/Y", $a);
    $name = $t[7];
    $result = str_replace(',', ' ', $name);
    if($t[8] == '') {
        continue;

    }
    $total_per_line = round(($t[9] * $t[8]));
    echo "<tr>";
    echo "<td>" . $new . "</td>";
    echo "<td>" . $t[1] . "</td>";
    echo "<td>" . $t[0] . "</td>";
    echo "<td>" . $t[6] . "</td>";
    echo "<td>" . $result . "</td>";
    echo "<td>" . $t[8] . "</td>";
    echo "<td>" . $t[9] . "</td>";
    echo "<td>" . $total_per_line . "</td>";
    echo "<td>" . $isArchived . "</td>";
    echo "<td>" . $isSent . "</td>";
    echo "<td>" . $iAux . "</td>";
    echo "</tr>";
    $sum+=$total_per_line;
    
 


}
echo "</table>";
echo '<h1>'.$sum.'<h1/>';
?>

<style>
@import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700');

$base-spacing-unit: 24px;
$half-spacing-unit: $base-spacing-unit / 2;

$color-alpha: #1772FF;
$color-form-highlight: #EEEEEE;

*, *:before, *:after {
	box-sizing:border-box;
}

body {
	padding:$base-spacing-unit;
	font-family:'Source Sans Pro', sans-serif;
	margin:0;
}

h1,h2,h3,h4,h5,h6 {
	margin:0;
}

.container {
	max-width: 1000px;
	margin-right:auto;
	margin-left:auto;
	display:flex;
	justify-content:center;
	align-items:center;
	min-height:100vh;
}

.table {
	width:100%;
	border:1px solid $color-form-highlight;
}

.table-header {
	display:flex;
	width:100%;
	background:#000;
	padding:($half-spacing-unit * 1.5) 0;
}

.table-row {
	display:flex;
	width:100%;
	padding:($half-spacing-unit * 1.5) 0;
	
	&:nth-of-type(odd) {
		background:$color-form-highlight;
	}
}

.table-data, .header__item {
	flex: 1 1 20%;
	text-align:center;
}

.header__item {
	text-transform:uppercase;
}

.filter__link {
	color:white;
	text-decoration: none;
	position:relative;
	display:inline-block;
	padding-left:$base-spacing-unit;
	padding-right:$base-spacing-unit;
	
	&::after {
		content:'';
		position:absolute;
		right:-($half-spacing-unit * 1.5);
		color:white;
		font-size:$half-spacing-unit;
		top: 50%;
		transform: translateY(-50%);
	}
	
	&.desc::after {
		content: '(desc)';
	}

	&.asc::after {
		content: '(asc)';
	}
	
}
</style>
<script>
    function downloadCSV(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV file
    csvFile = new Blob([csv], {type: "text/csv"});

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // Create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Hide download link
    downloadLink.style.display = "none";

    // Add the link to DOM
    document.body.appendChild(downloadLink);

    // Click download link
    downloadLink.click();
}
function exportTableToExcel(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
}

    function exportTableToCSV(filename) {
    var csv = [];
    var rows = document.querySelectorAll("table tr");
    
    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");
        
        for (var j = 0; j < cols.length; j++) 
            row.push(cols[j].innerText);
        
        csv.push(row.join(","));        
    }

    // Download CSV file
    downloadCSV(csv.join("\n"), filename);
}


</script>

</body>

</html>






    


	
